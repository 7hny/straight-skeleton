/*
 
 
 _________ ____  .-. _________/ ____ .-. ____ 
 __|__  (_)_/(_)(   )____<    \|    (   )  (_)
                 `-'                 `-'      


 art & game engine

 ____________________________________  ?   ____________________________________
                                     (._.)

 
 This file is part of darkcity package, the city map generator of polymorph
 engine.
 For the latest info, see http://polymorph.cool/
 
 Copyright (c) 2017 polymorph.cool
 
 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:
 
 The above copyright notice and this permission notice shall be included in
 all copies or substantial portions of the Software.
 
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 THE SOFTWARE.
 
 ascii font: rotated by MikeChat & myflix
 have fun and be cool :)
 
 ___________________________________( ^3^)_____________________________________
  
*/

/* 
 * File:   DCDot.h
 * Author: frankiezafe
 *
 * Created on April 27, 2017, 7:24 PM
 */

#ifndef DARKCITY_VEC3_H
#define DARKCITY_VEC3_H

#define _USE_MATH_DEFINES

#include <iostream>
#include <cmath>
#include "DC.h"

namespace darkcity {

    template< typename T >
    class Vec3 {
    public:
        
        const T& x;
        const T& y;
        const T& z;

        Vec3() :
        x(values[0]),
        y(values[1]),
        z(values[2]) {
        }

        Vec3(T ax, T ay, T az) :
        x(values[0]),
        y(values[1]),
        z(values[2]) {
            values[0] = ax;
            values[1] = ay;
            values[2] = az;
        }
        
        Vec3(const Vec3<T>& src) :
        x(values[0]),
        y(values[1]),
        z(values[2]) {
            values[0] = src.x;
            values[1] = src.y;
            values[2] = src.z;
        }

        virtual ~Vec3() {
        }

        inline void set(float ax, float ay, float az) {
            values[0] = ax;
            values[1] = ay;
            values[2] = az;
        }

        inline T length() const {
            return (T) sqrt(x * x + y * y + z * z);
        }

        inline T distance(const Vec3<T>& other) const {
            T vx = x - other.x;
            T vy = y - other.y;
            T vz = z - other.z;
            return (T) sqrt(vx * vx + vy * vy + vz * vz);
        }

        inline void cross(const Vec3<T>& other) {
            T _x = y * other.z - z * other.y;
            T _y = z * other.x - x * other.z;
            values[2] = x * other.y - y * other.x;
            values[1]  = _x;
            values[0] = _y;
        }

        inline Vec3<T> crossed(const Vec3<T>& other) const {
            T _x = y * other.z - z * other.y;
            T _y = z * other.x - x * other.z;
            return Vec3<T>(_x, _y, x * other.y - y * other.x);
        }

        inline T dot(const Vec3<T>& other) const {
            return x * other.x + y * other.y + z * other.z;
        }

        inline void normalize() {
            T length = (T) sqrt(x * x + y * y + z * z);
            if (length > 0) {
                values[0] /= length;
                values[1] /= length;
                values[2] /= length;
            }
        }

        inline Vec3<T> normalized() {
            Vec3<T> d;
            T length = (T) sqrt(x * x + y * y + z * z);
            if (length > 0) {
                d[0] = x / length;
                d[1] = y / length;
                d[2] = z / length;
            }
            return d;
        }

        inline bool operator==(const Vec3<T>& src) const {
            return (
                    values[0] == src.x &&
                    values[1] == src.y &&
                    values[2] == src.z
                    );
        }

        inline void operator+=(const T& v) {
            values[0] += v;
            values[1] += v;
            values[2] += v;
        }

        inline void operator+=(const Vec3<T>& src) {
            values[0] += src.x;
            values[1] += src.y;
            values[2] += src.z;
        }

        inline void operator-=(const T& v) {
            values[0] -= v;
            values[1] -= v;
            values[2] -= v;
        }

        inline void operator-=(const Vec3<T>& src) {
            values[0] -= src.x;
            values[1] -= src.y;
            values[2] -= src.z;
        }

        inline void operator*=(const T& v) {
            values[0] *= v;
            values[1] *= v;
            values[2] *= v;
        }

        inline void operator*=(const Vec3<T>& src) {
            values[0] *= src.x;
            values[1] *= src.y;
            values[2] *= src.z;
        }

        inline void operator/=(const T& v) {
            values[0] /= v;
            values[1] /= v;
            values[2] /= v;
        }

        inline void operator/=(const Vec3<T>& src) {
            values[0] /= src.x;
            values[1] /= src.y;
            values[2] /= src.z;
        }

        inline Vec3<T> operator+(const T& v) const {
            return Vec3<T> (
                    values[0] + v,
                    values[1] + v,
                    values[2] + v
                    );
        }
        
        inline Vec3<T> operator+(const Vec3<T>& src) const {
            return Vec3<T> (
                    values[0] + src.x,
                    values[1] + src.y,
                    values[2] + src.z
                    );
        }

        inline Vec3<T> operator-(const T& v) const {
            return Vec3<T> (
                    values[0] - v,
                    values[1] - v,
                    values[2] - v
                    );
        }

        inline Vec3<T> operator-(const Vec3<T>& src) const {
            return Vec3<T> (
                    values[0] - src.x,
                    values[1] - src.y,
                    values[2] - src.z
                    );
        }

        inline Vec3<T> operator*(const T& v) const {
            return Vec3<T> (
                    values[0] * v,
                    values[1] * v,
                    values[2] * v
                    );
        }

        inline Vec3<T> operator*(const Vec3<T>& src) const {
            return Vec3<T> (
                    values[0] * src.x,
                    values[1] * src.y,
                    values[2] * src.z
                    );
        }

        inline Vec3<T> operator/(const T& v) const {
            return Vec3<T> (
                    values[0] / v,
                    values[1] / v,
                    values[2] / v
                    );
        }

        inline Vec3<T> operator/(const Vec3<T>& src) const {
            return Vec3<T> (
                    values[0] / src.x,
                    values[1] / src.y,
                    values[2] / src.z
                    );
        }

        inline void operator=(const T& v) {
            values[0] = v;
            values[1] = v;
            values[2] = v;
        }

        inline void operator=(const Vec3<T>& src) {
            values[0] = src.x;
            values[1] = src.y;
            values[2] = src.z;
        }

        inline void operator=(const Vec3<T>* src) {
            values[0] = src->x;
            values[1] = src->y;
            values[2] = src->z;
        }

        inline Vec3<T> operator-() const {
            return Vec3<T>(-x, -y, -z);
        }

        inline T& operator[](uint8_t i) {
            return values[i];
        }
        
        inline float get(uint8_t i) {
            return values[i];
        }

        friend std::ostream& operator<<(std::ostream& os, const Vec3<T>& d) {
            os << d.x << ", " << d.y << ", " << d.z;
            return os;
        }

        friend std::istream& operator>>(std::istream& is, Vec3<T>& d) {
            is >> d.values[0];
            is.ignore(sizeof(T));
            is >> d.values[1];
            is.ignore(sizeof(T));
            is >> d.values[2];
            return is;
        }
        
        // definition of UP axis once and for all
        
        static Vec3<T> UP;
        static uint8_t axis1;
        static uint8_t axis2;
        static uint8_t upAxis;
        
        static void UPx() {
            UP.set( 1,0,0 );
            upAxis = 0;
            axis1 = 1;
            axis2 = 2;
        }
        static void UPx_inverse() {
            UP.set( -1,0,0 );
            upAxis = 0;
            axis1 = 1;
            axis2 = 2;
        }
        static void UPy() {
            UP.set( 0,1,0 );
            axis1 = 0;
            upAxis = 1;
            axis2 = 2;
        }
        static void UPy_inverse() {
            UP.set( 0,-1,0 );
            axis1 = 0;
            upAxis = 1;
            axis2 = 2;
        }
        static void UPz() {
            UP.set( 0,0,1 );
            axis1 = 0;
            axis2 = 1;
            upAxis = 2;
        }
        static void UPz_inverse() {
            UP.set( 0,0,-1 );
            axis1 = 0;
            axis2 = 1;
            upAxis = 2;
        }

    protected:

        T values[3];

    };
    
    // default is set for Ogre UP axis: Y
    template< typename T >
    Vec3<T> Vec3<T>::UP = Vec3<T>( 0,1,0 );
    template<typename T>
    uint8_t Vec3<T>::axis1 = 0;
    template<typename T>
    uint8_t Vec3<T>::axis2 = 2;
    template<typename T>
    uint8_t Vec3<T>::upAxis = 1;
    
    template< typename T, typename S >
    class Vec3Util {
        public:
            static Vec3<T> convert( const Vec3<S>& src ) {
                return Vec3<T>(
                        (T) src.x,
                        (T) src.y,
                        (T) src.z
                        );
            }
        
    };
    
};

#endif /* DARKCITY_VEC3_H */


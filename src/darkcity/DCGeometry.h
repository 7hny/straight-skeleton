/*
 
 
 _________ ____  .-. _________/ ____ .-. ____ 
 __|__  (_)_/(_)(   )____<    \|    (   )  (_)
                 `-'                 `-'      


 art & game engine

 ____________________________________  ?   ____________________________________
                                     (._.)

 
 This file is part of darkcity package, the city map generator of polymorph
 engine.
 For the latest info, see http://polymorph.cool/
 
 Copyright (c) 2017 polymorph.cool
 
 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:
 
 The above copyright notice and this permission notice shall be included in
 all copies or substantial portions of the Software.
 
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 THE SOFTWARE.
 
 ascii font: rotated by MikeChat & myflix
 have fun and be cool :)
 
 ___________________________________( ^3^)_____________________________________
  
 */

/* 
 * File:   DCGeometry.h
 * Author: frankiezafe
 *
 * Created on April 28, 2017, 7:56 PM
 */

#ifndef DARKCITY_GEOMETRY_H
#define DARKCITY_GEOMETRY_H

#include "DCVec3.h"

namespace darkcity {

    template<typename T>
    struct intersectionInfo {
        bool success;
        bool in_segment_a;
        bool in_segment_b;
        Vec3<T> result;
    };

    template<typename T>
    class Geometry {
    public:

        static T precision(T value = -1) {
            if (value >= 0) {
                _precision = value;
            }
            return _precision;
        }
        
        static T minimum_distance(T value = -1) {
            if (value >= 0) {
                _minimum_distance = value;
            }
            return _minimum_distance;
        }
        
        static T edgeevent_threshold(T value = -1) {
            if (value >= 0) {
                _edgeevent_threshold = value;
            }
            return _edgeevent_threshold;
        }

        static bool intersection(
                Vec3<T> a1, Vec3<T> a2,
                Vec3<T> b1, Vec3<T> b2,
                Vec3<T>& result,
                bool restrict_to_segment_a = false,
                bool restrict_to_segment_b = false,
                intersectionInfo<T>* info = 0
                ) {

            if (info) {
                info->result = 0.;
                info->success = false;
                info->in_segment_a = false;
                info->in_segment_b = false;
            }

            uint8_t axis1(Vec3<T>::axis1);
            uint8_t axis2(Vec3<T>::axis2);

            T a = a2[ axis1 ] - a1[ axis1 ];
            T b = b1[ axis1 ] - b2[ axis1 ];
            T c = a2[ axis2 ] - a1[ axis2 ];
            T d = b1[ axis2 ] - b2[ axis2 ];
            T e = b1[ axis1 ] - a1[ axis1 ];
            T f = b1[ axis2 ] - a1[ axis2 ];
            T denom = a * d - b * c;

            if (
                    (denom > 0 && denom < _precision) ||
                    (denom < 0 && denom > -_precision)
                    ) {

                // parrallel
                return false;

            } else {

                T t = (e * d - b * f) / denom;
                T s = (a * f - e * c) / denom;

                if (info) {
                    info->success = true;
                    if (t >= 0 && t <= 1) {
                        info->in_segment_a = true;
                    }
                    if (s >= 0 && s <= 1) {
                        info->in_segment_b = true;
                    }
                    info->result[ axis1 ] =
                            a1[ axis1 ] +
                            t * (a2[ axis1 ] - a1[ axis1 ]);
                    info->result[ axis2 ] =
                            a1[ axis2 ] +
                            t * (a2[ axis2 ] - a1[ axis2 ]);
                    info->result[ Vec3<T>::upAxis ] = 0;
                }

                if (
                        (!restrict_to_segment_a || (t >= 0 && t <= 1)) &&
                        (!restrict_to_segment_b || (s >= 0 && s <= 1))
                        ) {
                    result[ axis1 ] =
                            a1[ axis1 ] +
                            t * (a2[ axis1 ] - a1[ axis1 ]);
                    result[ axis2 ] =
                            a1[ axis2 ] +
                            t * (a2[ axis2 ] - a1[ axis2 ]);
                    result[ Vec3<T>::upAxis ] = 0;
                    return true;
                }

            }
            return false;
        }

    private:

        static T _precision;
        static T _minimum_distance;
        static T _edgeevent_threshold;

    };

    template<typename T>
    T Geometry<T>::_precision = T(1e-5);
    template<typename T>
    T Geometry<T>::_minimum_distance = T(1e-2);
    template<typename T>
    T Geometry<T>::_edgeevent_threshold = T(1e-2);


};

#endif /* DARKCITY_GEOMETRY_H */


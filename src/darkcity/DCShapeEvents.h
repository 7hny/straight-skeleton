/*
 
 
 _________ ____  .-. _________/ ____ .-. ____ 
 __|__  (_)_/(_)(   )____<    \|    (   )  (_)
                 `-'                 `-'      


 art & game engine

 ____________________________________  ?   ____________________________________
                                     (._.)

 
 This file is part of darkcity package, the city map generator of polymorph
 engine.
 For the latest info, see http://polymorph.cool/
 
 Copyright (c) 2017 polymorph.cool
 
 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:
 
 The above copyright notice and this permission notice shall be included in
 all copies or substantial portions of the Software.
 
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 THE SOFTWARE.
 
 ascii font: rotated by MikeChat & myflix
 have fun and be cool :)
 
 ___________________________________( ^3^)_____________________________________
  
 */

/* 
 * File:   DCShapeEvents.h
 * Author: frankiezafe
 *
 * Created on April 28, 2017, 9:26 PM
 */

#ifndef DARKCITY_SHAPEEVENTS_H
#define DARKCITY_SHAPEEVENTS_H

#include "DCPoint.h"

namespace darkcity {

    template< typename T >
    class SplitEvent {
    public:

        uint16_t pt_index;
        uint16_t segA_index;
        uint16_t segB_index;
        Vec3<T> event_position;
        Point<T>* pt;
        Point<T>* segA;
        Point<T>* segB;
        Vec3<T> seg_projection;
        T distance;
        T shrink;

        SplitEvent( ) :
        pt_index( 0 ),
        segA_index( 0 ),
        segB_index( 0 ),
        pt( 0 ),
        segA( 0 ),
        segB( 0 ),
        distance( 0 ),
        shrink( 0 ) {
        }

        inline void operator=( const SplitEvent& src ) {
            pt_index = src.pt_index;
            segA_index = src.segA_index;
            segB_index = src.segB_index;
            event_position = src.event_position;
            pt = src.pt;
            segA = src.segA;
            segB = src.segB;
            seg_projection = src.seg_projection;
            distance = src.distance;
            shrink = src.shrink;
        }

        inline void clear( ) {
            pt_index = 0;
            segA_index = 0;
            segB_index = 0;
            event_position = 0.;
            pt = 0;
            segA = 0;
            segB = 0;
            seg_projection = 0;
            distance = 0;
            shrink = 0;
        }

    };

    template< typename T >
    class EdgeEvent {
    public:

        Vec3<T> position;
        Point<T>* pt;
        T distance;
        // always positive!
        T shrink;

        EdgeEvent( ) :
        pt( 0 ),
        distance( 0 ),
        shrink( 0 ) {
        }

        inline void operator=( const EdgeEvent& src ) {
            position = src.position;
            pt = src.pt;
            distance = src.distance;
            shrink = src.shrink;
        }

        inline void clear( ) {
            position = 0.;
            pt = 0;
            distance = 0;
            shrink = 0;
        }

    };

};

#endif /* DARKCITY_SHAPEEVENTS_H */


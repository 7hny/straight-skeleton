/*
 
 
 _________ ____  .-. _________/ ____ .-. ____ 
 __|__  (_)_/(_)(   )____<    \|    (   )  (_)
                 `-'                 `-'      


 art & game engine

 ____________________________________  ?   ____________________________________
                                     (._.)

 
 This file is part of darkcity package, the city map generator of polymorph
 engine.
 For the latest info, see http://polymorph.cool/
 
 Copyright (c) 2017 polymorph.cool
 
 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:
 
 The above copyright notice and this permission notice shall be included in
 all copies or substantial portions of the Software.
 
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 THE SOFTWARE.
 
 ascii font: rotated by MikeChat & myflix
 have fun and be cool :)
 
 ___________________________________( ^3^)_____________________________________
  
 */

/* 
 * File:   DCPolygon.cpp
 * Author: frankiezafe
 * 
 * Created on May 7, 2017, 11:43 PM
 */

#include "DCPolygon.h"
#include "ofConstants.h"

using namespace darkcity;

template <typename T>
void Polygon<T>::link() {

    if (!points) {
        return;
    }

    for (
            uint16_t h = point_num - 1, i = 0, j = 1;
            i < point_num; ++h, ++i, ++j
            ) {

        if (h >= point_num) {
            h = 0;
        }
        if (j >= point_num) {
            j = 0;
        }

        points[i].previous = &points[h];
        points[i].next = &points[j];

    }

}

template <typename T>
void Polygon<T>::proximity_check() {

    // verify points too close from one another
    T min_dist = Geometry<T>::minimum_distance();
    uint16_t too_close = 0;
    bool* to_merge = new bool[ point_num ];

    // distance to next for each point
    for (uint16_t i = 0; i < point_num; ++i) {

        points[i].dir = (Vec3<T>) ((*points[i].next) - points[i]);
        to_merge[ i ] = false;

        if (points[i].dir.length() <= min_dist) {
            ++too_close;
            to_merge[ i ] = true;
        }

    }

    if (too_close > 0) {

#ifdef DARKCITY_POLYGON_DEBUG
        std::cout << "Polygon::proximity_check: " <<
                too_close << " point(s) will be merged" <<
                std::endl;
#endif

        // backup of previous positions
        uint16_t prev_point_num = point_num;
        Vec3<T> * prev_points = new Vec3<T>[ point_num ];
        for (uint16_t i = 0; i < point_num; ++i) {
            prev_points[i] = points[i];
        }

        // allocation of new number of points
        allocate(point_num - too_close);
        // linking
        link();

        // copy of previous location for far points
        for (uint16_t i = 0, u = 0; i < prev_point_num; ++i) {
            if (!to_merge[i]) {
                points[u] = prev_points[i];
                ++u;
            }
        }

        delete [] prev_points;

    }

    delete [] to_merge;

}

template <typename T>
void Polygon<T>::process(bool check_self_intersection) {

    //TODO
    //    proximity_check();

    if (!points) {
        return;
    }

    uint8_t ax1 = Vec3<T>::axis1;
    uint8_t ax2 = Vec3<T>::axis2;
    uint8_t uax = Vec3<T>::upAxis;

    center = 0.;

    T precision = Geometry<T>::precision();

    for (uint16_t last = point_num - 1, i = 0; i < point_num; ++i) {

        center += points[i] / point_num;

        // direction calculation
        if (i == 0) {
            // previous dir is mandatory for normal calculation
            points[last].dir = (Vec3<T>) ((*points[last].next) - points[last]);
            points[last].dir_2d = points[last].dir;
            points[last].dir_2d[uax] = 0;
            points[last].dir.normalize();
            points[last].dir_2d.normalize();
            points[last].dir_angle = atan2(
                    points[last].dir[ax2],
                    points[last].dir[ax1]
                    );
            points[last].dir_normal = points[last].dir.crossed(Vec3<T>::UP);
            points[last].dir_normal[uax] = 0;
            points[last].dir_normal.normalize();
        }
        if (i != last) {
            // last point has already been rendered
            points[i].dir = (Vec3<T>) ((*points[i].next) - points[i]);
            points[last].dir_2d = points[last].dir;
            points[last].dir_2d[uax] = 0;
            points[last].dir.normalize();
            points[last].dir_2d.normalize();
            points[i].dir_angle = atan2(
                    points[i].dir[ax2],
                    points[i].dir[ax1]
                    );
            points[i].dir_normal = points[i].dir.crossed(Vec3<T>::UP);
            points[i].dir_normal[uax] = 0;
            points[i].dir_normal.normalize();
        }

        // 2d version of 
        points[i].pos_2d = (Vec3<T>) points[i];
        points[i].pos_2d[uax] = 0;

        // point normal
        points[i].normal = (
                points[i].previous->dir_normal +
                points[i].dir_normal
                ) * 0.5;
        points[i].normal[uax] = 0;
        points[i].normal.normalize();

        // absolute normal angle
        points[i].normal_angle = atan2(
                points[i].normal[ax2],
                points[i].normal[ax1]);

        // relative normal angle
        points[i].angle = points[i].normal_angle - points[i].dir_angle;

        T sa = sin(points[i].angle);
        if (sa >= -precision && sa <= precision) {
            std::cout << "TOO BIG ANGLE TO BE KEPT!" << std::endl;
        }

        // shrink_multiplier, once and for all
        points[i].shrink_multiplier = 1 / sa;

        // concavity
        points[i].concav = (points[i].dir.dot(points[i].normal) > 0);

    }

    // events
    events(check_self_intersection);

}

template <typename T>
void Polygon<T>::reset_events() {

    preprocess_inflate = false;
    preprocess_shrink = false;
    edgeevent_in.pt = 0;
    edgeevent_out.pt = 0;
    splitevent_in_01 = 0;
    splitevent_in_02 = 0;
    splitevent_out_01 = 0;
    splitevent_out_02 = 0;

    if (edgeevents_in) {
        for (uint16_t u = 0; u < point_num; ++u) {
            if (edgeevents_in[u]) {
                delete edgeevents_in[u];
                edgeevents_in[u] = 0;
            }
            if (edgeevents_out[u]) {
                delete edgeevents_out[u];
                edgeevents_out[u] = 0;
            }
            if (splitevents_in[u]) {
                delete splitevents_in[u];
                splitevents_in[u] = 0;
            }
            if (splitevents_out[u]) {
                delete splitevents_out[u];
                splitevents_out[u] = 0;
            }
        }
    }
}

template <typename T>
void Polygon<T>::clear_events() {

    preprocess_inflate = false;
    preprocess_shrink = false;
    splitevent_in_01 = 0;
    splitevent_in_02 = 0;
    splitevent_out_01 = 0;
    splitevent_out_02 = 0;

    if (edgeevents_in) {

        for (uint16_t u = 0; u < point_num; ++u) {
            if (edgeevents_in[u]) {
                delete edgeevents_in[u];
            }
            if (edgeevents_out[u]) {
                delete edgeevents_out[u];
            }
            if (splitevents_in[u]) {
                delete splitevents_in[u];
            }
            if (splitevents_out[u]) {
                delete splitevents_out[u];
            }
        }

        delete [] edgeevents_in;
        delete [] edgeevents_out;
        delete [] splitevents_in;
        delete [] splitevents_out;
        edgeevents_in = 0;
        edgeevents_out = 0;
        splitevents_in = 0;
        splitevents_out = 0;

    }
}

template <typename T>
void Polygon<T>::allocate_events() {

    clear_events();

    if (point_num == 0) {
        return;
    }

    edgeevents_in = new EdgeEvent<T>*[point_num];
    edgeevents_out = new EdgeEvent<T>*[point_num];
    splitevents_in = new SplitEvent<T>*[point_num];
    splitevents_out = new SplitEvent<T>*[point_num];
    for (uint16_t u = 0; u < point_num; ++u) {
        edgeevents_in[u] = 0;
        edgeevents_out[u] = 0;
        splitevents_in[u] = 0;
        splitevents_out[u] = 0;
    }

}

template <typename T>
void Polygon<T>::events(bool check_self_intersection) {

    self_intersection = false;
    if (check_self_intersection) {
        self_intersection = perimeter_intersection(true);
    }

    reset_events();

    Vec3<T> ee;
    Vec3<T> iee;

    // edge events
    for (uint16_t i = 0, j = 1; i < point_num; ++i, ++j) {

        j %= point_num;

        if (Geometry<T>::intersection(
                points[i].pos_2d,
                points[i].pos_2d + (points[i].normal * 100),
                points[j].pos_2d,
                points[j].pos_2d + (points[j].normal * 100),
                ee, false, false
                )
                ) {

            iee = ee - points[i].pos_2d;
            iee.normalize();

            T dot = points[i].normal.dot(iee);
            bool inside = (dot < 0);
            bool outside = (dot > 0);

            if (inside) {

                edgeevents_in[i] = new EdgeEvent<T>();
                edgeevents_in[i]->position = ee;
                edgeevents_in[i]->pt = &points[i];
                edgeevents_in[i]->distance = ee.distance(points[i].pos_2d);
                edgeevents_in[i]->shrink =
                        edgeevents_in[i]->distance /
                        -points[i].shrink_multiplier;

                if (
                        edgeevent_in.pt == 0 ||
                        edgeevent_in.shrink > edgeevents_in[i]->shrink
                        ) {
                    edgeevent_in = (*edgeevents_in[i]);
                }

            }

            if (outside) {

                edgeevents_out[i] = new EdgeEvent<T>();
                edgeevents_out[i]->position = ee;
                edgeevents_out[i]->pt = &points[i];
                edgeevents_out[i]->distance = ee.distance(points[i].pos_2d);
                edgeevents_out[i]->shrink =
                        edgeevents_out[i]->distance /
                        -points[i].shrink_multiplier;

                if (
                        edgeevent_out.pt == 0 ||
                        edgeevent_out.shrink > edgeevents_out[i]->shrink
                        ) {
                    edgeevent_out = (*edgeevents_out[i]);
                }

            }

        }

    }

    // no split event below 5 points
    // at 4 points, edge events will occurs simultaneously with split events
    if (point_num < 5) {
        return;
    }

    // split events
    for (
            uint16_t
            u = 0,
            u_skip0 = (point_num - 2) % point_num,
            u_skip1 = (point_num - 1) % point_num,
            u_skip2 = 1;
            u < point_num;
            ++u,
            ++u_skip0,
            ++u_skip1,
            ++u_skip2
            ) {

        u_skip0 %= point_num;
        u_skip1 %= point_num;
        u_skip2 %= point_num;

        Vec3<T> un = points[u].pos_2d + points[u].normal * 10;

        for (
                uint16_t i = 0, j = 1;
                i < point_num; ++i, ++j) {

            j %= point_num;

            if (
                    i == u_skip0 ||
                    i == u_skip1 ||
                    i == u_skip2 ||
                    i == u
                    ) {
                continue;
            }

            // testing each normal on all segments, except adjacent
            if (directed_split_event(
                    u, // index of current point
                    i, j, // index of points of segment
                    un // far points of bisector
                    )) {


            }

        }

        shortest_split_event(u);

    }

}

template <typename T>
void Polygon<T>::shortest_split_event(uint16_t current_index) {

    SplitEvent<T>* sp_e = 0;
    bool inward = true;

    if (splitevents_in[current_index]) {
        sp_e = splitevents_in[current_index];
    } else if (splitevents_out[current_index]) {
        sp_e = splitevents_out[current_index];
        inward = false;
    }

    if (!sp_e) {
        return;
    }

    // detection of smallest split event
    if (
            (
            inward &&
            (
            !splitevent_in_01 ||
            splitevent_in_01->shrink > sp_e->shrink
            )
            ) ||
            (
            !inward &&
            (
            !splitevent_out_01 ||
            splitevent_out_01->shrink > sp_e->shrink
            )
            )
            ) {

        // transfert of splitevent_out_01 to splitevent_out_02
        // only if it is smaller than the splitevent_out_02 or
        // if it is not already set
        if (
                (
                inward &&
                (
                !splitevent_in_02 ||
                splitevent_in_02->shrink > splitevent_in_01->shrink
                )
                ) ||
                (
                !inward &&
                (
                !splitevent_out_02 ||
                splitevent_out_02->shrink > splitevent_out_01->shrink
                )
                )
                ) {

            if (inward) {
                splitevent_in_02 = splitevent_in_01;
            } else {
                splitevent_out_02 = splitevent_out_01;
            }

        }

        if (inward) {
            splitevent_in_01 = sp_e;
        } else {
            splitevent_out_01 = sp_e;
        }

    } else if (
            (
            inward &&
            (
            !splitevent_in_02 ||
            splitevent_in_02->shrink > sp_e->shrink
            )
            ) ||
            (
            !inward &&
            (
            !splitevent_out_02 ||
            splitevent_out_02->shrink > sp_e->shrink
            )
            )
            ) {

        if (inward) {
            splitevent_in_02 = sp_e;
        } else {
            splitevent_out_02 = sp_e;
        }

    }

}

template <typename T>
bool Polygon<T>::directed_split_event(
        uint16_t current_index,
        uint16_t segmenta_index1,
        uint16_t segmenta_index2,
        Vec3<T> pu_and_normal
        ) {

    Point<T>& pu = points[ current_index ];
    Point<T>& pi = points[ segmenta_index1 ];
    Point<T>& pj = points[ segmenta_index2 ];

    intersectionInfo<T> ii;
    Geometry<T>::intersection(
            pi.pos_2d,
            pj.pos_2d,
            pu.pos_2d,
            pu_and_normal,
            ii.result,
            false, false,
            &ii);

    if (!ii.success) {
        return false;
    }

    // verification of intersection position
    if (pu.concav && (ii.result - pu.pos_2d).dot(pu.normal) > 0) {
        // inner split must happen inside the shape...
        return false;
    } else if (!pu.concav && (ii.result - pu.pos_2d).dot(pu.normal) < 0) {
        // outer split must happen outside the shape...
        return false;
    }

    // important part of the calculation:
    // slit event depends on the angles formed by the angle bisector of u
    // and the segment. Once identify, we can project the intersection 
    // perpendicularly on the segment and deduce the maximum shrink factor
    // see http://frankiezafe.org/index.php?title=WorkUnit:Disrupted_Cities_-_road_network#Split_events
    T intersecta = pi.dir_angle - pu.normal_angle;
    T d = pu.pos_2d.distance(ii.result);
    T dr = d / (1 + (1 / sin(pu.angle)) * (sin(intersecta)));
    T dd = d - dr;
    T max_factor = -dr * sin(intersecta);
    if (max_factor < 0) {
        return false;
    }

    // selecting the inner or outer split event depending on concavity
    SplitEvent<T>* s_event = 0;
    if (pu.concav) {
        s_event = splitevents_in[current_index];
    } else {
        s_event = splitevents_out[current_index];
    }

    Vec3<T> position;

    if (pu.concav) {
        position = pu.pos_2d + pu.normal * -dd;
    } else {
        position = pu.pos_2d + pu.normal * dd;
    }

    // does one of the normals of the segment cross the bisector between
    // the intersection point and the split event point?
    // if yes, the segment will out of collision area before pu reached it
    if (!ii.in_segment_a) {

        // moving the segment by the max_factor
        // and checking the status if there is an intersection
        T fac = max_factor;
        if (!pu.concav) {
            fac *= -1;
        }
        Vec3<T> moved_i = pi.pos_2d +
                pi.normal *
                pi.shrink_multiplier *
                fac;
        Vec3<T> moved_j = pj.pos_2d +
                pj.normal *
                pj.shrink_multiplier *
                fac;

        if (!Geometry<T>::intersection(
                moved_i,
                moved_j,
                pu.pos_2d,
                pu_and_normal,
                ii.result,
                true, false)) {

            return false;

        }

    }

    // if a segment has already been found, we have
    // to compare the maximum shrink factor of this intersection
    // with the previous one
    if (
            s_event &&
            max_factor > s_event->shrink
            ) {
        return false;
    }

    if (!s_event) {
        if (pu.concav) {
            splitevents_in[current_index] = new SplitEvent<T>();
            s_event = splitevents_in[current_index];
        } else {
            splitevents_out[current_index] = new SplitEvent<T>();
            s_event = splitevents_out[current_index];
        }

    }

    // storing basic data
    s_event->pt_index = current_index;
    s_event->pt = &pu;
    s_event->segA = &pi;
    s_event->segB = &pj;
    s_event->segA_index = segmenta_index1;
    s_event->segB_index = segmenta_index2;
    s_event->distance = d;
    s_event->shrink = max_factor;

    // rendering compound data
    if (pu.concav) {
        s_event->event_position = position;
        s_event->seg_projection =
                s_event->event_position +
                pi.dir_normal * max_factor;
    } else {
        s_event->event_position = position;
        s_event->seg_projection =
                s_event->event_position -
                pi.dir_normal * max_factor;
    }

    if (pu.concav) {
        // verification of indices
        // projection of points in split event factor
        Vec3<T> split_pt = (*s_event->pt).pos_2d +
                s_event->pt->normal *
                s_event->pt->shrink_multiplier *
                s_event->shrink;
        Vec3<T> split_segA = (*s_event->segA).pos_2d +
                s_event->segA->normal *
                s_event->segA->shrink_multiplier *
                s_event->shrink;
        Vec3<T> split_segB = (*s_event->segB).pos_2d +
                s_event->segB->normal *
                s_event->segB->shrink_multiplier *
                s_event->shrink;
        T AB_dist = split_segA.distance(split_segB);

        if (split_pt.distance(split_segA) / AB_dist > 1) {

            // event happens AFTER current segment => 
            // moving all indices to next segment
            ++s_event->segA_index %= point_num;
            ++s_event->segB_index %= point_num;
            s_event->segA = &points[ s_event->segA_index ];
            s_event->segB = &points[ s_event->segB_index ];

        } else if (split_pt.distance(split_segB) / AB_dist > 1) {

            // event happens BEFORE current segment =>
            // moving all indices to previous segment
            s_event->segA_index = (s_event->segA_index + (point_num - 1)) % point_num;
            s_event->segB_index = (s_event->segB_index + (point_num - 1)) % point_num;
            s_event->segA = &points[ s_event->segA_index ];
            s_event->segB = &points[ s_event->segB_index ];

        }
    }

    return true;

}

template <typename T>
bool Polygon<T>::perimeter_intersection(bool breakonfirst) {

    bool detected = false;
    Vec3<T> ee;

    for (uint16_t i = 0, j = 1; i < point_num; ++i, ++j) {

        if (j >= point_num) {
            j = 0;
        }

        for (uint16_t u = 0, v = 1; v < point_num; ++u, ++v) {

            if (v >= point_num) {
                v = 0;
            }

            if (u == i || v == i || u == j || v == j) {
                continue;
            }

            if (Geometry<T>::intersection(
                    points[i].pos_2d,
                    points[j].pos_2d,
                    points[u].pos_2d,
                    points[v].pos_2d,
                    ee, true, true
                    )
                    ) {
                detected = true;
                if (breakonfirst) {
                    break;
                }
            }

        }

        if (detected && breakonfirst) {
            break;
        }

    }


    return detected;

}

template <typename T>
void Polygon<T>::flip_normal() {

    for (uint16_t i = 0; i < point_num; ++i) {
        points[i].normal *= -1;

        // normal angle
        points[i].normal_angle += M_PI;

        // absolute angle
        points[i].angle = points[i].normal_angle - points[i].dir_angle;

        // shrink_multiplier, once and for all
        points[i].shrink_multiplier = 1 / sin(points[i].angle);

        // concavity
        points[i].concav = (points[i].dir_2d.dot(points[i].normal) > 0);

    }

}

template <typename T>
void Polygon<T>::edgeevent_in_ptnum(uint16_t& ptn, T factor) {

    ptn = point_num;
    if (factor <= 0) {
        return;
    }
    for (uint16_t i = 0; i < point_num; ++i) {
        if (
                edgeevents_in[ i ] &&
                evaluate_edgeevent(edgeevents_in[ i ], factor)
                ) {
            --ptn;
        }
    }

}

template <typename T>
void Polygon<T>::edgeevent_out_ptnum(uint16_t& ptn, T factor) {

    ptn = point_num;
    for (uint16_t i = 0; i < point_num; ++i) {
        if (
                factor > 0 &&
                edgeevents_out[ i ] &&
                evaluate_edgeevent(edgeevents_out[ i ], factor)
                ) {
            --ptn;
        }
    }

}

template <typename T>
void Polygon<T>::splitevent_in_ptnum(uint16_t& ptn1, uint16_t& ptn2) {

    // no split in in this polygon, why asking?
    if (!splitevent_in_01) {
        ptn1 = 0;
        ptn2 = 0;
        return;
    }

    if (splitevent_in_01->pt_index < splitevent_in_01->segA_index) {
        ptn1 =
                (1 + splitevent_in_01->segA_index) -
                splitevent_in_01->pt_index;
    } else {
        ptn1 =
                (1 + point_num) -
                (splitevent_in_01->pt_index - splitevent_in_01->segA_index);
    }
    ptn2 = (point_num + 1) - ptn1;

}

template <typename T>
void Polygon<T>::splitevent_out_ptnum(uint16_t& ptn1, uint16_t& ptn2) {

    // no split out in this polygon, why asking?
    if (!splitevent_out_01) {
        ptn1 = 0;
        ptn2 = 0;
        return;
    }

    if (splitevent_out_01->pt_index < splitevent_out_01->segA_index) {
        ptn1 =
                (1 + splitevent_out_01->segA_index) -
                splitevent_out_01->pt_index;
    } else {
        ptn1 =
                (1 + point_num) -
                (splitevent_out_01->pt_index - splitevent_out_01->segA_index);
    }

    ptn2 = (point_num + 1) - ptn1;

}

template <typename T>
void Polygon<T>::shrink(
        T factor,
        Polygon<T>* polygon1,
        Polygon<T>* polygon2
        ) {

    if (factor == 0) {
        (*polygon1) = (*this);
        polygon1->link();
        polygon1->process(false);
        return;
    }

    bool eevent = false;
    bool sevent = false;
    T smallest_factor = factor;

    if (factor > 0) {

        if (
                edgeevent_in.pt &&
                smallest_factor >= edgeevent_in.shrink
                ) {

            smallest_factor = edgeevent_in.shrink;
            eevent = true;

        } else if (
                point_num > 4 &&
                splitevent_in_01 &&
                smallest_factor >= splitevent_in_01->shrink
                ) {

            smallest_factor = splitevent_in_01->shrink;
            sevent = true;

        }

    } else {

        smallest_factor *= -1;

        if (
                edgeevent_out.pt &&
                smallest_factor >= edgeevent_out.shrink
                ) {

            smallest_factor = edgeevent_out.shrink;
            eevent = true;

        } else if (
                point_num > 4 &&
                splitevent_out_01 &&
                smallest_factor >= splitevent_out_01->shrink
                ) {

            smallest_factor = splitevent_out_01->shrink;
            sevent = true;

        }

    }

    if (eevent && point_num <= 3) {
#ifdef DARKCITY_POLYGON_DEBUG
        std::cout << "\tno shrink applied: " <<
                point_num << " in " <<
                polygon1 << std::endl;
#endif
        return;
    }

    if (!eevent && !sevent) {

        // no events, simple shrink
        
#ifdef DARKCITY_POLYGON_DEBUG
        std::cout << "\tsimple shrink: " <<
                point_num << " in " <<
                polygon1 << std::endl;
#endif

        // if polygon is already allocated correctly, 
        // no need to change anything
        if (polygon1->point_num != point_num) {
            if (polygon1->point_num != 0) {
                polygon1->clear();
            }
            polygon1->allocate(point_num);
        }

        for (uint16_t i = 0; i < point_num; ++i) {
            (*polygon1)[i] = points[i] +
                    points[i].normal *
                    points[i].shrink_multiplier *
                    factor;
        }

        polygon1->link();
        polygon1->process(false);

    } else if (eevent) {
        
        // edge event detected

        uint16_t p1_ptnum = point_num;
        bool* skips = new bool[ point_num ];

        for (uint16_t i = 0; i < point_num; ++i) {

            if (
                    factor > 0 &&
                    edgeevents_in[ i ] &&
                    evaluate_edgeevent(edgeevents_in[ i ], factor)
                    ) {

                skips[i] = true;
                --p1_ptnum;

            } else if (
                    factor < 0 &&
                    edgeevents_out[ i ] &&
                    evaluate_edgeevent(edgeevents_out[ i ], factor)
                    ) {

                skips[i] = true;
                --p1_ptnum;

            } else {

                skips[i] = false;

            }
        }

#ifdef DARKCITY_POLYGON_DEBUG
        std::cout << "\tedge event: " <<
                point_num << " > " <<
                p1_ptnum << std::endl;
#endif

        // if polygon is already allocated correctly, 
        // no need to change anything
        if (polygon1->point_num != p1_ptnum) {
            if (polygon1->point_num != 0) {
                polygon1->clear();
            }
            polygon1->allocate(p1_ptnum);
        }

        if (factor < 0) {
            smallest_factor *= -1;
        }
        
        uint16_t skipped = 0;
        T up = 0;
        
        for (uint16_t i = 0, j = 0; i < point_num; ++i) {
            if (!skips[i]) {
                uint16_t previ = i + ( point_num - 1 );
                previ %= point_num;
                up = 0;
                while( skips[previ] ) {
                    up += points[previ][Vec3<T>::upAxis];
                    previ = (previ + ( point_num - 1 )) % point_num;
                    skipped++;
                }
                (*polygon1)[j] = points[i] +
                        points[i].normal *
                        points[i].shrink_multiplier *
                        smallest_factor;
                // averaging up axis
                if ( skipped > 0 ) {
                    (*polygon1)[j][Vec3<T>::upAxis] += up;
                    (*polygon1)[j][Vec3<T>::upAxis] /= (skipped+1);
                    skipped = 0;
                }
                ++j;
            }
        }

        polygon1->link();
        polygon1->process(false);

        delete [] skips;

    } else if (sevent) {
        
        // split event detected!

        uint16_t p1_ptnum;
        uint16_t p2_ptnum;

        if (factor > 0) {
            splitevent_in_ptnum(p1_ptnum, p2_ptnum);
        } else {
            splitevent_out_ptnum(p1_ptnum, p2_ptnum);
        }

#ifdef DARKCITY_POLYGON_DEBUG        
        std::cout << "\tsplit event: " <<
                p1_ptnum << " & " <<
                p2_ptnum << " in " <<
                polygon1 << " & " <<
                polygon2 << std::endl;
#endif

        // if polygons are already allocated correctly, 
        // no need to change anything
        if (polygon1->point_num != p1_ptnum) {
            if (polygon1->point_num != 0) {
                polygon1->clear();
            }
            polygon1->allocate(p1_ptnum);
        }

        if (polygon2 && polygon2->point_num != p2_ptnum) {
            if (polygon2->point_num != 0) {
                polygon2->clear();
            }
            polygon2->allocate(p2_ptnum);
        }

        // working values
        uint16_t start_index;
        uint16_t seg_index;
        uint16_t segEnd_index;
        uint16_t index;

        if (factor < 0) {
            smallest_factor *= -1;
            start_index = splitevent_out_01->pt_index;
            seg_index = splitevent_out_01->segA_index;
        } else {
            start_index = splitevent_in_01->pt_index;
            seg_index = splitevent_in_01->segA_index;
        }

        // processing polygon 1

        index = start_index;
        for (uint16_t i = 0; i < p1_ptnum; ++i) {
            index %= point_num;
            (*polygon1)[i] = points[index] +
                    points[index].normal *
                    points[index].shrink_multiplier *
                    smallest_factor;
            if ( i == 0 ) {
                (*polygon1)[i][ Vec3<T>::upAxis ] += (
                        points[seg_index] +
                        points[seg_index].normal *
                        points[seg_index].shrink_multiplier *
                        smallest_factor
                        )[ Vec3<T>::upAxis ];
                (*polygon1)[i][ Vec3<T>::upAxis ] *= 0.5;
            }
            ++index;
        }

        polygon1->link();
        polygon1->process(false);
        pass_split_event(start_index, polygon1);

        if (!polygon2) {
            return;
        }

        // processing polygon 2

        if (factor < 0) {
            start_index = splitevent_out_01->segB_index;
            seg_index = splitevent_out_01->pt_index;
            segEnd_index = splitevent_out_01->segA_index;
        } else {
            start_index = splitevent_in_01->segB_index;
            seg_index = splitevent_in_01->pt_index;
            segEnd_index = splitevent_in_01->segA_index;
        }

        index = start_index;
        for (uint16_t i = 0; i < p2_ptnum; ++i) {
            index %= point_num;
            (*polygon2)[i] = points[index] +
                    points[index].normal *
                    points[index].shrink_multiplier *
                    smallest_factor;
            if ( i == 0 ) {
                (*polygon2)[i][ Vec3<T>::upAxis ] += (
                        points[seg_index] +
                        points[seg_index].normal *
                        points[seg_index].shrink_multiplier *
                        smallest_factor
                        )[ Vec3<T>::upAxis ];
                (*polygon2)[i][ Vec3<T>::upAxis ] *= 0.5;
            } else if ( index == seg_index ) {
                (*polygon2)[i][ Vec3<T>::upAxis ] += (
                        points[segEnd_index] +
                        points[segEnd_index].normal *
                        points[segEnd_index].shrink_multiplier *
                        smallest_factor
                        )[ Vec3<T>::upAxis ];
                (*polygon2)[i][ Vec3<T>::upAxis ] *= 0.5;
            }
            ++index;
        }

        polygon2->link();
        polygon2->process(false);
        pass_split_event(start_index, polygon2);

    }

}

template <typename T>
void Polygon<T>::pass_split_event(
        uint16_t start_index,
        Polygon<T>* polygon
        ) {

    if (!splitevent_in_02 && !splitevent_out_02) {
        return;
    }

    uint8_t in_transfer(0);
    uint16_t in_pt_index(0);
    uint16_t in_seg_index(UINT16_MAX);
    bool in_foundA = false;

    uint8_t out_transfer(0);
    uint16_t out_pt_index(0);
    uint16_t out_seg_index(UINT16_MAX);
    bool out_foundA = false;

    uint16_t index = start_index;
    uint16_t polygon_ptnum = polygon->point_num;

    for (uint16_t i = 0; i < polygon_ptnum; ++i) {
        index %= point_num;
        if (in_transfer < 2 && splitevent_in_02) {
            if (splitevent_in_02->pt_index == index) {
                in_pt_index = i;
                ++in_transfer;
            } else if (
                    in_seg_index == UINT16_MAX &&
                    splitevent_in_02->segA_index == index) {
                in_seg_index = i;
                ++in_transfer;
                in_foundA = true;
            } else if (
                    in_seg_index == UINT16_MAX &&
                    splitevent_in_02->segB_index == index) {
                in_seg_index = i;
                ++in_transfer;
                in_foundA = false;
            }
        }
        if (out_transfer < 2 && splitevent_out_02) {
            if (splitevent_out_02->pt_index == index) {
                out_pt_index = i;
                ++out_transfer;
            } else if (
                    out_seg_index == UINT16_MAX &&
                    splitevent_out_02->segA_index == index) {
                out_seg_index = i;
                ++out_transfer;
                out_foundA = true;
            } else if (
                    out_seg_index == UINT16_MAX &&
                    splitevent_out_02->segB_index == index) {
                out_seg_index = i;
                ++out_transfer;
                out_foundA = false;
            }
        }
        ++index;
    }

    if (in_transfer == 2) {

        SplitEvent<T> tmp;
        tmp = (*splitevent_in_02);
        tmp.shrink -= splitevent_in_01->shrink;
        tmp.pt_index = in_pt_index;
        if (in_foundA) {
            tmp.segA_index = in_seg_index;
            tmp.segB_index = (in_seg_index + 1) % polygon_ptnum;
        } else {
            tmp.segA_index = (in_seg_index + (polygon_ptnum - 1)) % polygon_ptnum;
            tmp.segB_index = in_seg_index;
        }
        polygon->splitevent_inject(tmp, true);

    }

    if (out_transfer == 2) {

        SplitEvent<T> tmp;
        tmp = (*splitevent_out_02);
        tmp.shrink -= splitevent_out_01->shrink;
        tmp.pt_index = out_pt_index;
        if (out_foundA) {
            tmp.segA_index = out_seg_index;
            tmp.segB_index = (out_seg_index + 1) % polygon_ptnum;
        } else {
            tmp.segA_index = (out_seg_index + (polygon_ptnum - 1)) % polygon_ptnum;
            tmp.segB_index = out_seg_index;
        }
        polygon->splitevent_inject(tmp, false);

    }

}

template <typename T>
bool Polygon<T>::splitevent_inject(SplitEvent<T>& src, bool in) {

    SplitEvent<T>** sp_array = splitevents_out;
    if (in) {
        sp_array = splitevents_in;
    }

    if (!sp_array) {

#ifdef DARKCITY_POLYGON_DEBUG
        std::cout << "Polygon.splitevent_inject: "
                "splitevents array is not defined!" <<
                std::endl;
#endif
        return false;

    }

    if (
            src.pt_index >= point_num ||
            src.segA_index >= point_num ||
            src.segB_index >= point_num
            ) {

#ifdef DARKCITY_POLYGON_DEBUG
        std::cout << "Polygon.splitevent_inject: "
                "wrong indices, " <<
                src.pt_index << ", " <<
                src.segA_index << ", " <<
                src.segB_index << " <> " <<
                point_num <<
                std::endl;
#endif
        return false;

    }

    if (!sp_array[ src.pt_index ]) {

#ifdef DARKCITY_POLYGON_DEBUG
        std::cout << "Polygon.splitevent_inject: "
                "no event yet for this point, creating one, " <<
                src.pt_index <<
                std::endl;
#endif
        sp_array[ src.pt_index ] = new SplitEvent<T>();

    } else if (sp_array[ src.pt_index ]->shrink < src.shrink) {

#ifdef DARKCITY_POLYGON_DEBUG
        std::cout << "Polygon.splitevent_inject: "
                "src.shrink is bigger, " <<
                sp_array[ src.pt_index ]->shrink << " <> " <<
                src.shrink <<
                std::endl;
#endif
        return false;

    }

    (*sp_array[ src.pt_index ]) = src;
    sp_array[ src.pt_index ]->pt = &points[ src.pt_index ];
    sp_array[ src.pt_index ]->segA = &points[ src.segA_index ];
    sp_array[ src.pt_index ]->segB = &points[ src.segB_index ];

    // verifying that injected event is not the smallest one
    if (
            in && (
            !splitevent_in_01 ||
            splitevent_in_01->shrink > src.shrink
            )
            ) {
        splitevent_in_02 = splitevent_in_01;
        splitevent_in_01 = sp_array[ src.pt_index ];
    } else if (
            !in && (
            !splitevent_out_01 ||
            splitevent_out_01->shrink > src.shrink
            )
            ) {
        splitevent_out_02 = splitevent_out_01;
        splitevent_out_01 = sp_array[ src.pt_index ];
    }

#ifdef DARKCITY_POLYGON_DEBUG
    std::cout << "Polygon.splitevent_inject: "
            "successfull injection on point " <<
            sp_array[ src.pt_index ]->pt_index << ", " <<
            sp_array[ src.pt_index ]->segA_index << ", " <<
            sp_array[ src.pt_index ]->segB_index << ", " <<
            "shrink: " << src.shrink << std::endl <<
            "\tinward: " << in << std::endl <<
            "\t" << "in: " << splitevents_in[ src.pt_index ] <<
            std::endl <<
            "\t" << "in first: " << splitevent_in_01 << ", " <<
            splitevent_in_01->shrink <<
            std::endl <<
            "\t" << "out: " << splitevents_out[ src.pt_index ] <<
            std::endl <<
            "\t" << "out first: " << splitevent_out_01 << ", " <<
            splitevent_out_01->shrink <<
            std::endl;
#endif

    return true;

}

template <typename T>
void Polygon<T>::projected_center(Vec3<T>& result) {

    result = center;
    if (point_num != 3) {
        return;
    }

    Geometry<T>::intersection(
            points[0], points[0] + points[0].normal * 10,
            points[1], points[1] + points[1].normal * 10,
            result,
            false, false);
    return;

}

template class Polygon<float>;
template class Polygon<double>;
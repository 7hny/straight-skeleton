/*
 
 
 _________ ____  .-. _________/ ____ .-. ____ 
 __|__  (_)_/(_)(   )____<    \|    (   )  (_)
                 `-'                 `-'      


 art & game engine

 ____________________________________  ?   ____________________________________
                                     (._.)

 
 This file is part of darkcity package, the city map generator of polymorph
 engine.
 For the latest info, see http://polymorph.cool/
 
 Copyright (c) 2017 polymorph.cool
 
 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:
 
 The above copyright notice and this permission notice shall be included in
 all copies or substantial portions of the Software.
 
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 THE SOFTWARE.
 
 ascii font: rotated by MikeChat & myflix
 have fun and be cool :)
 
 ___________________________________( ^3^)_____________________________________
  
 */

/* 
 * File:   DCPolygon.h
 * Author: frankiezafe
 *
 * Created on May 7, 2017, 11:43 PM
 */

#ifndef DARKCITY_POLYGON_H
#define DARKCITY_POLYGON_H

#include "DCPoint.h"
#include "DCGeometry.h"
#include "DCShapeEvents.h"

namespace darkcity {

    template< typename T >
    class Polygon {
    public:

        uint16_t point_num;
        Point<T> * points;
        Vec3<T> center;
        bool self_intersection;
        bool preprocess_inflate;
        bool preprocess_shrink;

        EdgeEvent<T>** edgeevents_in;
        EdgeEvent<T>** edgeevents_out;
        EdgeEvent<T> edgeevent_in;
        EdgeEvent<T> edgeevent_out;

        SplitEvent<T>** splitevents_in;
        SplitEvent<T>** splitevents_out;
        SplitEvent<T>* splitevent_in_01;
        SplitEvent<T>* splitevent_in_02;
        SplitEvent<T>* splitevent_out_01;
        SplitEvent<T>* splitevent_out_02;

        Polygon() :
        point_num(0),
        points(0),
        self_intersection(false),
        preprocess_inflate(false),
        preprocess_shrink(false),
        edgeevents_in(0),
        edgeevents_out(0),
        splitevents_in(0),
        splitevents_out(0),
        splitevent_in_01(0),
        splitevent_in_02(0),
        splitevent_out_01(0),
        splitevent_out_02(0) {
            center = 0.;
        }

        ~Polygon() {
            clear();
        }

        inline void clear() {
            clear_events();
            if (points) {
                delete [] points;
            }
            point_num = 0;
            points = 0;
            center = 0.;
            self_intersection = false;
            preprocess_inflate = false;
            preprocess_shrink = false;
            edgeevent_in.clear();
            edgeevent_out.clear();
        }

        /* Set the number of points in the polygon and set the memory
         * in the appropriate way. There is no other way to setup a polygon,
         * this method is the first one to be called after instantiation.
         * @param
         *      unsigned int specifying the number of points to create
         */
        inline void allocate(uint16_t i) {
            if (point_num != i && point_num > 0) {
                clear();
            }
            point_num = i;
            if (i == 0) {
                return;
            }
            points = new Point<T>[i];
            allocate_events();
        }

        /* Simple way to acccess any points in the polygon.
         * @param
         *      index of the points
         * @return
         *      reference to the point
         * @remark
         *      there is NO check on the validity of the index, check
         *      that param is lesser than point_num before calling.
         */
        inline Point<T>& operator[](uint16_t i) const {
            return points[ i ];
        }

        /* Partial copy of the polygon. Call link & process after copy.
        */
        void operator=(const Polygon& src) {

            clear();
            allocate(src.point_num);
            for (uint16_t i = 0; i < point_num; ++i) {
                points[i] = src[i];
            }

        }

        /* After allocation, it is mandatory to call this method to 
         * create the relations between points. All other methods
         * are based on the assumption linkage is done.
         */
        void link();

        /* After allocation and linkage, call this method to create all
         * the events required for splitting and merging polygon while 
         * shrinking.
         * @param
         *      set to false if you are certain that the polygon has no
         *      intersection with itself, meaning that none of its segment
         *      cross another
         */
        void process( bool check_self_intersection = true );

        /* This method is called automatically at the end of the process().
         * Call manually if a regeneration of events is required.
         * @param
         *      set to false if you are certain that the polygon has no
         *      intersection with itself, meaning that none of its segment
         *      cross another
         */
        void events( bool check_self_intersection = true );

        /* This method verify if the polygon has self intersections.
         * It is called automatically at the beginning of process() by default.
         * @param
         *      set to true if you need the total count of intersections
         */
        bool perimeter_intersection(bool breakonfirst);

        /* Invert all the normales of the polygon
         */
        void flip_normal();

        /* Retrieval of the remaining number of points for a positive shrink
         * factor.
         * @param
         *      reference to a unsigned int that will contain the remaining
         *      number of points
         * @param
         *      factor of shrink to simulate
         */
        void edgeevent_in_ptnum(uint16_t& ptn, T factor);

        /* Retrieval of the remaining number of points for a negative shrink
         * factor.
         * @param
         *      reference to a unsigned int that will contain the remaining
         *      number of points
         * @param
         *      factor of shrink to simulate
         */
        void edgeevent_out_ptnum(uint16_t& ptn, T factor);

        /* Retrieval of the number of points of the two polygons to create
         * when meeting the inward first split event. The closest inward split 
         * event is used for the calculation. If the polygon has no inward 
         * split event, both unsigned int will be 0
         * @param
         *      reference to a unsigned int that will contain the first part
         *      of the polygon
         * @param
         *      reference to a unsigned int that will contain the second part
         *      of the polygon
         */
        void splitevent_in_ptnum(uint16_t& ptn1, uint16_t& ptn2);

        /* Retrieval of the number of points of the two polygons to create
         * when meeting the outward first split event. The closest outward split 
         * event is used for the calculation. If the polygon has no outward 
         * split event, both unsigned int will be 0
         * @param
         *      reference to a unsigned int that will contain the first part
         *      of the polygon
         * @param
         *      reference to a unsigned int that will contain the second part
         *      of the polygon
         */
        void splitevent_out_ptnum(uint16_t& ptn1, uint16_t& ptn2);

        /* Apply the requested shrink factor on the first or both specified
         * polygon. Create result polygons before calling this method. They will
         * be allocated if the number of points is 0 or not correctly fixed.
         * link() and process() methods will be called automatically.
         * Pass a positive factor to shrink (inward)
         * Pass a negative factor to inflate (outward)
         * @param
         *      shrink factor to apply
         * @param
         *      pointer to a Polygon<T>
         * @param
         *      pointer to a Polygon<T>
         */
        void shrink(
                T factor,
                Polygon<T>* polygon1,
                Polygon<T>* polygon2);

        void projected_center(Vec3<T>& result);

        /* Injecting an inward or outward split event manually in polygon.
         * Passed event must pass the correct indices (pt_index, segA_index & 
         * segB_index), the pointers and other validity checks will 
         * be performed before storing the event.
         * @param 
         *      a split event specifying the correct point indices
         * 
         * @remark 
         *      do this AFTER process() call. During process, all
         *      events are cleared.
         *      Define DARKCITY_POLYGON_DEBUG in DC.h to enable debug info
         */
        bool splitevent_inject(SplitEvent<T>& src, bool in);

    protected:

        void reset_events();

        void clear_events();

        void allocate_events();

        void proximity_check();
        
        void shortest_split_event( uint16_t current_index );
        
        bool directed_split_event(
                uint16_t current_index,
                uint16_t segmenta_index1,
                uint16_t segmenta_index2,
                Vec3<T> current_bisector);

        void pass_split_event(
            uint16_t start_index,
            Polygon<T>* polygon
            );
        
        static bool evaluate_edgeevent( EdgeEvent<T>* ev, T factor ) {
             
            if ( factor > 0 ) {
                return ev->shrink - factor <=
                    Geometry<T>::edgeevent_threshold();
            } else if ( factor < 0 ) {
                return ev->shrink + factor <=
                    Geometry<T>::edgeevent_threshold();
            } else {
                return false;
            }
        }
        
    };

};

#endif /* DARKCITY_POLYGON_H */

